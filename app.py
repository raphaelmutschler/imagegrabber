import os
import sys

base_path = os.path.dirname(os.path.abspath(__file__))

# Insert local directories into path
sys.path.append(os.path.join(base_path, 'lib'))

#import the providers
from app.provider.deviant import Deviantart
from app.provider.flickr import Flickr

#initialize the Deviantart Scrapper, scrap my deviantpage
x = Deviantart("raphaelmutschlr", username=False, password=False, update=False)
x.parse()

#initialize the Flickr Scrapper, scrap my flickr
y = Flickr("raphaelmutschlr", username=False, password=False, update=False)
y.parse()

#downloads will be stored relative to the path where you run the script:
#downloads/PROVIDERNAME/USER_TO_SCRAP/
