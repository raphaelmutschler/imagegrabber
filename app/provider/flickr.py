import re
import os
import sys
import mechanize

from urllib2 import URLError, HTTPError
from httplib import IncompleteRead


from bs4 import BeautifulSoup
from app.logger import logger

class Flickr(object):
    def __init__(self, flickname, username=None, password=None, update=False):
        self.name = "flickr"
        self.username = username
        self.password = password
        self.flickname = flickname
        self.browser = mechanize.Browser()
        self.browser.set_handle_robots(False)

        self.only_new = update
        self.max_exist = 10
        self.exist_count = 0

        self.pages = []
        self.failed_links = []
        self.total_pages = 0
        self.current_page = 0
        self.total_images = 0
        self.current_image = 0

        self.baseurl = "http://www.flickr.com/photos/%s" % self.flickname



        if self.username and self.password:
            self._do_login()
        else:
            logger.warning( "running without login! mature content is NOT supported!" )


    def _do_login(self):
        logger.info( "sending login informations to %s" % self.name )

        self.browser.open('https://www.deviantart.com/users/login', "ref=http%3A%2F%2Fwww.deviantart.com%2F&remember_me=1")
        self.browser.select_form(nr=1)
        self.browser.form['username'] = self.username
        self.browser.form['password'] = self.password
        self.browser.submit()
        result = self.browser.response().read()

        if self.username in result:
            logger.info( "login successfull" )
        else:
            logger.error( "login failed" )
            logger.warning( "running without login! mature content is NOT supported!" )

    def _parse_pages(self):
        """ Parses all gallery pages """
        pages = []
        pattern = "/photos/%s/[0-9_-]*" % self.flickname
        for i in range(0,13/1,1):
            url = self.baseurl + "/page" + str(i)
            html = self._get_url(url)
            prelim = re.findall(pattern, html, re.IGNORECASE|re.DOTALL)

            c = len(prelim)
            for match in prelim:
                if match in pages:
                    c -= 1
                else:
                    pages.append(match)

            done = re.findall("(This section has no deviations yet!|This collection has no items yet!)", html, re.IGNORECASE|re.S)

            if len(done) >= 1 or c <= 0:
                break

            logger.debug( "%s's gallery page %s crawled..." % ( self.flickname, (i/24)+1 ) )

        self.pages = pages
        self.total_pages = len(pages)

        if self.total_pages == 0:
            logger.info( "%s's gallery had no deviations.")
            return 0
        else:
            logger.info( "total deviations in %s's gallery found:%s" % ( self.flickname, self.total_pages ) )
            return self.pages


    def _get_url(self, url):
        remaining_tries = 3
        while 1:
            try:
                f = self.browser.open(url)
                return str(f.read())
            except HTTPError, e:
                #print "HTTP Error: ", e.code , url
                logger.warning( "HTTP Error:: %s %s" % (e.code, url) )
                remaining_tries -= 1
                if remaining_tries == 0:
                    raise
            except URLError, e:
                #print "URL Error: ", e.reason , url
                logger.warning( "URL Error: %s %s" % (e.reason, url) )
                remaining_tries -= 1
                if remaining_tries == 0:
                    raise
            except IncompleteRead:
                logger.warning( "Incomplete read: %s" % url )
                remaining_tries -= 1
                if remaining_tries == 0:
                    raise

    def _download(self, link, filename):
        cur_dir = os.path.join("downloads", self.name, self.flickname)
        if not os.path.exists(cur_dir):
            os.makedirs(cur_dir)

        filename = os.path.join(cur_dir, filename)

        if os.path.exists(filename):
            logger.info( "%s already exists. skipping" % filename )
            self.exist_count += 1
            return

        local_file = open(filename, "wb")
        local_file.write(self._get_url(link))
        local_file.close()

    def _get_links(self, link):
        """ trys to get the downloadink on the single art page """
        link = "http://flickr.com" + link + "/sizes/o/"
        html = self._get_url(link)
        soup = BeautifulSoup(html)
        filelink = None
        filename = None

        if "Mature Content Filter</a> is On" in html:
            logger.error( "Mature Content Filter is On. Please login and disable it" )
            return (None, None)

        try:
            req = soup.find(text=re.compile( r".*Download the Original size of this photo.*" ))
            filelink = req["href"]
            filename = filelink.split("/")[-1].split("?")[0]
            return (filename, filelink)
        except:
            logger.debug( "download link not found, falling back to preview image" )
            req = soup.find('img', attrs={'src': re.compile( r".*staticflickr\.com/[0-9]*/.*" ) })
            filelink = req['src']

            if filelink:
                filename = os.path.basename(filelink)
                return (filename, filelink)
            else:
                self.failed_links.append(link)
                return (None, None)

    def _save_failed(self):
        path = os.path.join(self.flickname, "error.txt")
        f = open(path, "w")
        for link in self.failed_links:
            f.write("%s\n" % link)
        f.close()

    def parse(self):
        if self.only_new:
            logger.info( "running in update mode, will stop after %s existing images where found" % self.max_exist )
        self._parse_pages()
        if self.total_pages > 0:
            for link in self.pages:
                self.current_page += 1
                logger.info( "Downloading %s of %s ( '%s' )" % ( self.current_page, self.total_pages, link ) )
                filename = ""
                filelink = ""
                if self.exist_count > self.max_exist and self.only_new:
                    logger.info( "found %s pictures already exist, stopped" % self.max_exist )
                    break
                try:
                    filename, filelink = self._get_links(link)
                    self._download(filelink, filename)
                except:
                    logger.error( "download error!" )
                    continue

            logger.info( "%s's gallery sucessfully ripped." % self.flickname)

        if self.failed_links:
            logger.info( "%s links failed, saved to %s/error.txt" % (len(self.failed_links), self.flickname) )
            self._save_failed()
