import logging
import logging.handlers

ERROR = logging.ERROR
WARNING = logging.WARNING
MESSAGE = logging.INFO
DEBUG = logging.DEBUG

LOG_FILENAME = "grabber.log"

logger = logging.getLogger("ImageGrabber")
formatter = logging.Formatter('%(asctime)s %(levelname)-8s: %(message)s', '%d.%m.%Y %H:%M:%S')

rotation = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=5 * 1024 * 1024, backupCount=5)
rotation.setFormatter(formatter)
logger.addHandler(rotation)


console = logging.StreamHandler()
console.setFormatter(formatter)
logger.addHandler(console)
logger.setLevel(logging.DEBUG)
